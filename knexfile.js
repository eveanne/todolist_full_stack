require("dotenv").config();
var pg = require("pg");
pg.defaults.ssl = true;

module.exports = {
  production: {
    client: "pg",
    connection: process.env.DATABASE_URL,
    dialectOptions: {
      ssl: {
        require: true,
        // Ref.: https://github.com/brianc/node-postgres/issues/2009
        rejectUnauthorized: false,
      },
      keepAlive: true,
    },
    migrations: {
      tableName: "knex_migrations",
      directory: `${__dirname}/migrations`,
    },
  },
};
