module.exports = {
  statusEnum: {
    A_FAIRE: "A faire",
    EN_COURS: "En cours",
    FAIT: "Fait",
  },
};
