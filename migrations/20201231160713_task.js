const { statusEnum } = require("../constants/status");

const statuses = Object.values(statusEnum);

exports.up = (knex) =>
  knex.schema.createTable("task", (table) => {
    table.increments();
    table.string("name").notNullable();
    table.enu("status", statuses).defaultTo(statusEnum.A_FAIRE);
    table.dateTime("deadline");
    table.timestamps();
  });

exports.down = (knex) => knex.schema.dropTable("task");
