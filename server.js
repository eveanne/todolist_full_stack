const express = require("express");
const signale = require("signale");
const app = express();
const knex = require("./database");

// app.use(express.static(path.join(__dirname, "./client/bluid")));

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "client/public/", "index.html"));
});

app.listen(4000, () => signale.success(`Server lancé sur le port ${4000}`));
